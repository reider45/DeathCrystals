package me.reider45.Deathcrystals;

import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public void onEnable()
	{
		saveDefaultConfig();

		Bukkit.getPluginManager().registerEvents(new Events(this), this);
	}

	public void onDisable()
	{
		Events.crystals.clear();
	}
	
	public void dropCrystal(Location drop, List<ItemStack> items, Player dead)
	{
		ItemStack crystal = new ItemStack(Material.NETHER_STAR, 1);

		ItemMeta meta = crystal.getItemMeta();
		meta.setDisplayName( getConfig().getString("Item.Name").replace("$", "�").replace("PLAYER", dead.getName()) );

		List<String> lore = new ArrayList<String>();
		lore.add( ChatColor.translateAlternateColorCodes('$', getConfig().getString("Item.Lore")).replace("%PLAYER%", dead.getName()) );
		meta.setLore(lore);
		crystal.setItemMeta(meta);
		
		Inventory inventory = Bukkit.createInventory(dead, InventoryType.CHEST, ChatColor.translateAlternateColorCodes('$', getConfig().getString("Inventory.Title")));
		
		for(ItemStack i : items){
			inventory.addItem(i);
		}
		
		drop.getWorld().dropItemNaturally(drop, crystal);
		
		Events.crystals.put(crystal.toString(), inventory);
	}

}
